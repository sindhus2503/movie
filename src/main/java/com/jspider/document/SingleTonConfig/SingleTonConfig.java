package com.jspider.document.SingleTonConfig;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SingleTonConfig {
	
	private static SessionFactory sessionFactory=null;

	private SingleTonConfig(){}
	
	
	public static SessionFactory getsessionFactory()
	
	{
		Configuration configuration = new Configuration();
		configuration.configure();
		SessionFactory sessionFactory1 = configuration.buildSessionFactory();
		if(sessionFactory==null)return sessionFactory=sessionFactory1;
		return sessionFactory;
		
		
	}

}
