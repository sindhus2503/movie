package com.jspiders.maven.constants;

 public interface  AppConstants {
	 public static String MOVIE_INFO="movie_info";
	 public static String COUNTRY_INFO="country_info";
	 public static String PM_INFO="primeminister_info";
	 public static String COMPANY_INFO="company_details";
	 public static String DEPARTMENT_INFO="department_details";
	 public static String COLLEGE_INFO="college_details";
	 public static String STUDENT_INFO="student_details";
	 

}
