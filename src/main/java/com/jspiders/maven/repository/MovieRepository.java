package com.jspiders.maven.repository;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import com.jspider.document.SingleTonConfig.SingleTonConfig;
import com.jspiders.maven.Entity.Movie;


public class MovieRepository {
	public void saveMovieDetails(Movie movie) 
	{
		try {
//			Configuration cfg=new Configuration();
//			cfg.configure();
		//	SessionFactory sessionFactory= (SessionFactory(SingleTonConfig.getsessionFactory())).
			Session session = SingleTonConfig.getsessionFactory().openSession();
			Transaction	transaction=session.beginTransaction();
			session.save(movie);
			transaction.commit();
			
		}	
		catch(HibernateException e)
	{	
			e.printStackTrace();
	}
	}
	
	
	
	public Movie getMovieById(Long id)
	{
		Configuration cfg=new Configuration();
		cfg.configure();
		SessionFactory sessionFactory=cfg.buildSessionFactory();
		Session session	=((SessionFactory)(SingleTonConfig.getsessionFactory())).openSession();
		Transaction	transaction=session.beginTransaction(); 
		Movie m=session.get(Movie.class, id);	
		return m;
	}
	
	
	
	public void updateRatingAndBudgetById(String rating,Double budget,Long id)
	{
		Movie m=getMovieById(id);
		if(m!=null);
		
		m.setRating(rating);
		m.setBudget(budget);
		Configuration cfg=new Configuration();
		cfg.configure();
		SessionFactory sessionFactory=cfg.buildSessionFactory();
		Session session	=sessionFactory.openSession();
		Transaction	transaction=session.beginTransaction(); 


		session.update(m);
		transaction.commit();
		System.out.println("updated successfully");
	}
	
	
	
	
	
	public void deleteById(Long id) {
		
		Movie m = getMovieById(id);
		
		if(m!=null) {
			Configuration cfg = new Configuration();
			cfg.configure();
			SessionFactory sessionFactory = cfg.buildSessionFactory();
			Session session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			session.delete(m);
			transaction.commit();
			System.out.println("Record deleted ");
		}
		else System.out.println("no record found");
	}

	
	
	
	
public List<Movie> findAll()
{
	Configuration configuration = new Configuration();
	configuration.configure();
	SessionFactory sessionFactory = configuration.buildSessionFactory();
	Session session = sessionFactory.openSession();
	String hql="from Movie";
	Query query = session.createQuery(hql);
	return query.list();
}
	
	
public Movie findByName(String movieName)
{
	Configuration configuration = new Configuration();
	configuration.configure();
	SessionFactory sessionFactory = configuration.buildSessionFactory();
	Session session = sessionFactory.openSession();
	String hql="from Movie where name=:mName";
	Query query = session.createQuery(hql);
	query.setParameter("mName",movieName);
	return (Movie) query.uniqueResult();
	}
	


	
public void updateRatingAndBudgetByName(String name,String rating,Double budget)
{
	Configuration configuration = new Configuration();
	configuration.configure();
	SessionFactory sessionFactory = configuration.buildSessionFactory();
	Session session = sessionFactory.openSession();
	Transaction transaction = session.beginTransaction();
	String hql="update Movie set rating=:r,budget=:d where name=:n";
	Query query = session.createQuery(hql);
	query.setParameter("n", name);
	query.setParameter("r", rating);
	query.setParameter("d", budget);
	int rowsUpdated=query.executeUpdate();
	transaction.commit();
	if(rowsUpdated>0)
	{
		System.out.println("update successfull");
	}else {
		System.out.println("not rows updated");
	}
}



	public void deleteByName(String name)
	{
		
		Configuration configuration = new Configuration();
		configuration.configure();
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		
		String hql = "delete Movie where name=:mName";
		Query query= session.createQuery(hql);
		query.setParameter("mName", name);
		query.executeUpdate();
		
		transaction.commit();
		System.out.println("record deleted successfully");
		}
		
		
	}


	


















