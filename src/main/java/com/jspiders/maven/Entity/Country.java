package com.jspiders.maven.Entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.jspiders.maven.constants.AppConstants;


@Entity
@Table(name=AppConstants.COUNTRY_INFO)

public class Country implements Serializable{

	@Id
	@GenericGenerator(name="c_info",strategy="increment")
	@GeneratedValue(generator="c_info")
	@Column(name="id")
	private Long id;
	
	@Column(name="name")
	private String name;
	
	
	@Column(name="capital")
	private String capital;
	
	@Column(name="population")
	private Long population;
	
	@Column(name="area")
	private  String area;
	
	@Column(name="continent")
	private String continent;

	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="fro_key")
	private PrimeMinister primeMinister;
	
	
	public Country()
	{
		
	}
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCapital() {
		return capital;
	}

	public void setCapital(String capital) {
		this.capital = capital;
	}

	public Long getPopulation() {
		return population;
	}

	public void setPopulation(Long population) {
		this.population = population;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getContinent() {
		return continent;
	}

	public void setContinent(String continent) {
		this.continent = continent;
	}


	public PrimeMinister getPrimeMinister() {
		return primeMinister;
	}


	public void setPrimeMinister(PrimeMinister primeMinister) {
		this.primeMinister = primeMinister;
	}
	
	
	
}
