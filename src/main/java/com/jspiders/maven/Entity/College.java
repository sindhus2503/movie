package com.jspiders.maven.Entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.jspiders.maven.constants.AppConstants;
@Entity
@Table(name=AppConstants.COLLEGE_INFO)
public class College implements Serializable{
    @Id
    @GenericGenerator(name="co_auto",strategy="increment")
    @GeneratedValue(generator="co_auto")
	@Column(name="id")
	private Long id;
	
	@Column(name="collegeName")
	private String collegeName;
	
	@Column(name="principal")
	private String principal;
	
	@Column(name="address")
	private String address;

	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCollegeName() {
		return collegeName;
	}

	public void setCollegeName(String collegeName) {
		this.collegeName = collegeName;
	}

	public String getPrincipal() {
		return principal;
	}

	public void setPrincipal(String principal) {
		this.principal = principal;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	
		
}
