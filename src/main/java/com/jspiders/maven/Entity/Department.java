package com.jspiders.maven.Entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.jspiders.maven.constants.AppConstants;

@Entity
@Table(name=AppConstants.DEPARTMENT_INFO)
public class Department implements Serializable{
	
    @Id
    @GenericGenerator(name="d_info",strategy="increment")
    @GeneratedValue(generator="d_info")
	@Column(name="id")
	private Long id;
	
	@Column(name="name")
	private String name;
	
	
	@Column(name="managerName")
	private String managerName;
	
	@Column(name="noOfEmployee")
	private Long noOfEmployee;
	
	
	
	public Department()
	{
		
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getManagerName() {
		return managerName;
	}


	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}


	public Long getNoOfEmployee() {
		return noOfEmployee;
	}


	public void setNoOfEmployee(Long noOfEmployee) {
		this.noOfEmployee = noOfEmployee;
	}
	
	
}
