package com.jspiders.maven.dto;

import java.io.Serializable;

public class StudInfo  implements Serializable
{
private Long id;
private String name;
private Long age;
private String Contact;
private String email;

public StudInfo() {}


public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public Long getAge() {
	return age;
}
public void setAge(Long age) {
	this.age = age;
}
public String getContact() {
	return Contact;
}
public void setContact(String contact) {
	Contact = contact;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}


public static void main(String[] args) {
	
}

}
